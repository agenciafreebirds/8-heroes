(function () {
  function labelVisibility () {
    var label = document.getElementById('label-email')
    var scrollY = window.scrollY || document.documentElement.scrollTop

    if (scrollY > 20) {
      label.classList.add('fade-out')
    } else {
      label.classList.remove('fade-out')
    }
  }

  window.addEventListener('scroll', labelVisibility)

  function vanillaAjax (obj) {
    if(obj.url === undefined || obj.method === undefined || obj.success === undefined) {
      throw new Error('The url, method, and success parameters are required in the vanillaAjax({url: "", method: "", success: ""}). Visit https://github.com/ederssouza/vanillajs-helpers for more information.');

    } else {
      var xhr = new XMLHttpRequest();
      xhr.open(obj.method, obj.url, true);

      // success request
      xhr.addEventListener('load', obj.success);

      // before request
      if(obj.before !== undefined) xhr.addEventListener('loadstart', obj.before);

      // complete request
      if(obj.complete !== undefined) xhr.addEventListener('loadend', obj.complete);

      // error
      if(obj.error !== undefined) xhr.addEventListener('error', obj.error);

      if(obj.method === 'POST'){
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send(obj.data);

      } else if(obj.method === 'GET') {
        xhr.send();
      }
    }
  }

  var formMsg = document.getElementById('form-msg');
  var inputEmail = document.getElementById('input-email');
  var inputEmail = document.getElementById('input-email');
  var form = document.getElementById('form-newsletter');
  var button = form.querySelector('button[type="submit"]');

  var newsletter = function (e) {
    e.preventDefault();
    var regex = /^([\w-]\.?)+@([\w-]+\.)+([A-Za-z]{2,4})+$/;
    formMsg.style.display = 'none';

    if (!regex.test(inputEmail.value)) {
      formMsg.style.display = 'block';
      formMsg.innerHTML = 'Invalid email address.';
      return
    }

    vanillaAjax({
      url: 'dts/newsletter.php',
      method: 'POST',
      data: 'email='+encodeURIComponent(JSON.stringify(inputEmail.value)),
      success: function(){
        var request = this;
        formMsg.style.display = 'block';

        if (request.status >= 200 && request.status < 400) {
          const normalize = /.*({.+?}).*/.exec(request.responseText)
          var data = JSON.parse(normalize[1]);

          if (data.status === 'success') {
            inputEmail.value = '';
            showModal();
          } else {
            formMsg.innerHTML = data.message;
          }
        } else {
          formMsg.innerHTML = 'The server received the request, but returned error.'
        }
      },
      before: function(){
        button.setAttribute('disabled', true);
        button.innerHTML = 'Loading...';
      },
      complete: function(){
        button.removeAttribute('disabled');
        button.innerHTML = 'Send';
      },
      error: function(){
        formMsg.style.display = 'block';
        formMsg.innerHTML = 'The server received the request, but returned error.'
      }
    });

    return false;
  };

  form.addEventListener('submit', newsletter)

  inputEmail.addEventListener('focus', function () {
    formMsg.style.display = 'none';
  });

  function showModal () {
    document.body.classList.add('modal-open');
  }

  function hideModal () {
    document.body.classList.remove('modal-open');
  }

  var close = document.getElementById('modal-close');
  close.addEventListener('click', hideModal)
})();