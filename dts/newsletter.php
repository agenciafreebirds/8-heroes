<?php
if ($_POST) {
  require_once('conn.php');

  $email = $_POST['email'];
  $email = str_replace('"', '', $email);

  if (!preg_match("/^([\w-]\.?)+@([\w-]+\.)+([A-Za-z]{2,4})+$/", $email)) {
    echo json_encode(array('status' => 'error', 'message' => 'Invalid email address.'));
    exit;
  } else {
    $conn = new mysqli($host, $user, $pass, $db);

    if($conn->connect_error) {
      echo json_encode(array('status' => 'error', 'message' => $conn->connect_error));
      exit;
    }

    $results = $conn->query("SELECT * FROM tb_newsletter WHERE `email` = '$email'");

    if ($results->num_rows > 0) {
      echo json_encode(array('status' => 'error', 'message' => 'E-mail already registered.'));

    } else {
      $stmt = $conn->prepare("INSERT INTO tb_newsletter (email) VALUES (?)");
      $stmt->bind_param("s", $userEmail);
      $userEmail = $email;
      $stmt->execute();

      echo json_encode(array('status' => 'success', 'message' => 'E-mail registered successfully.'));
    }
  }

  exit;
} else {
  echo json_encode(array('status' => 'error', 'message' => 'There was a problem trying again later.'));
  exit;
}
?>
