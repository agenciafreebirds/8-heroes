<?php

$pattern = "/mysql:\/\/(.+):(.+)@(.+)\/(.+)\?/";
$env = getenv("CLEARDB_DATABASE_URL");

if ($env) {
  preg_match_all($pattern, $env, $matches);

  $host = $matches[3][0];
  $user = $matches[1][0];
  $pass = $matches[2][0];
  $db   = $matches[4][0];
} else {
  $host = "";
  $user = "";
  $pass = "";
  $db   = "";
}
