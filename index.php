<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>8 Herores</title>
    <meta name="description" content="8 HEROES is a We-Learning community created to help you successfully navigate your path to leadership and personal development.">
    <link rel="stylesheet" href="dist/css/app.min.css">
    <link rel="shortcut icon" href="dist/img/favicon.png" type="image/png">
  </head>

  <body>
    <header class="header">
      <div class="container">
        <h1 class="header-brand">
          <img src="dist/svg/8heroes.svg" alt="8 Heroes brand" title="8 Heroes">
        </h1>

        <div class="header-content">
          <h2 class="header-title">
            We Believe<br class="visible-xs">
            in the<br class="visible-md visible-lg"> power of<br class="visible-xs">
            Stories
          </h2>

          <form id="form-newsletter" class="header-form" novalidate>
            <label for="input-email" id="label-email">
            Curious to check out our stories?<br>
            Join our community for updates and early access.
            </label>
            <input type="email" id="input-email" placeholder="Your Email Address" autocomplete="off">
            <button type="submit">Join now</button>
            <span id="form-msg" class="form-msg"></span>
          </form>
        </div>
      </div><!-- /.container -->
    </header><!-- /.header -->

    <section class="section services">
      <div class="container">
        <h2 class="sr-only">Services</h2>

        <p class="services-desc">We-Learning Community</p>
        <p class="services-desc2">Transformative Learning Through<br> Storytelling.</p>

        <div class="services-list">
          <div class="row">
            <div class="col-sm-4">
              <div class="service-item content">
                <h3 class="services-item-title">New Content<br> Launching<br> Every Week</h3>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="service-item watch">
                <h3 class="services-item-title">Watch 8h<br> anywhere<br> at anytime</h3>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="service-item learn">
                <h3 class="services-item-title">Learn,<br> Connect<br> &amp; Inspire</h3>
              </div>
            </div>
          </div>
        </div>
      </div><!-- /.container -->
    </section><!-- /.services -->

    <section class="section coming">
      <div class="container">
        <h2 class="coming-title">Coming soon</h2>

        <img src="dist/img/content/banner-mobile.jpg" alt="" title="" class="visible-xs">
        <img src="dist/img/content/banner.jpg" alt="" title="" class="hidden-xs">

        <p class="coming-desc">
          Tyler King, a senior product manager discovered<br class="visible-md visible-lg">
          how effective communication is the foundation of any relationship.<br class="visible-md visible-lg">
          Find out how to transform the way you connect and<br class="visible-md visible-lg">
          communicate watching our first episode <span>Communication is the New Black</span>.</p>
      </div>
    </section><!-- /.coming -->

    <footer class="footer">
      <div class="container">
        <h2 class="footer-title">Keep up with 8h</h2>

        <ul class="footer-medias">
          <li>
            <a href="https://www.facebook.com/The8Heroes" title="Facebook" target="_blank">
              <img src="dist/img/content/facebook.png" alt="Facebook icon" title="Facebook">
            </a>
          </li>
          <li>
            <a href="https://twitter.com/_8Heroes" title="Twitter" target="_blank">
              <img src="dist/img/content/twitter.png" alt="Twitter icon" title="Twitter">
            </a>
          </li>
          <li>
            <a href="https://www.instagram.com/8_heroes" title="Instagram" target="_blank">
              <img src="dist/img/content/instagram.png" alt="Instagram icon" title="Instagram">
            </a>
          </li>
          <li>
            <a href="https://www.youtube.com/channel/UCeodByA_pahxJ7dSpxTQfRw" title="YouTube" target="_blank">
              <img src="dist/img/content/youtube.png" alt="YouTube icon" title="YouTube">
            </a>
          </li>
        </ul>

        <div class="footer-contact">
          <a href="mailto:8@8heroes.com" title="Contact us" class="footer-contact-title">Contact us</a>
          <span class="footer-contact-copyright">Copyright &copy; <?php echo date('Y'); ?> 8Heroes</span>
        </div>
      </div>
    </footer><!-- /.footer -->

    <div class="modal">
      <header class="modal-header">
        <h4 class="modal-title">Oh, Hey There!</h4>
      </header>

      <div class="modal-body">
        <p>Thank you for joining<br> our community!</p>
        <p><small>8 HEROES is coming.<br> Talk Soon... ;)</small></p>

        <button id="modal-close" class="modal-close">Close</button>
      </div>
    </div><!-- /.modal -->

    <script src="dist/js/app.js"></script>
  </body>
</html>
